#!/bin/bash
set -e

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" <<-EOSQL
    CREATE TABLE h500 (val JSONB, dat DATE);

    CREATE TABLE merd (val JSONB, dat DATE);

    CREATE TABLE t850 (val JSONB, dat DATE);

    CREATE TABLE prec (val JSONB, dat DATE);

    INSERT INTO
        h500 (val, dat)
    SELECT
        array_to_json(
            array(
                SELECT
                    floor(random() * (7000 -2000 + 1) + 2000) :: int
                FROM
                    generate_series(0, 144 * 73 -1)
            )
        ),
        dd.dat
    FROM
        generate_series (
            '2018-01-01' :: timestamp,
            '2020-01-01' :: timestamp,
            '1 day' :: INTERVAL
        ) dd(dat);

    INSERT INTO
        merd (val, dat)
    SELECT
        array_to_json(
            array(
                SELECT
                    floor(random() * (7000 -2000 + 1) + 2000) :: int
                FROM
                    generate_series(0, 144 * 73 -1)
            )
        ),
        dd.dat
    FROM
        generate_series (
            '2018-01-01' :: timestamp,
            '2020-01-01' :: timestamp,
            '1 day' :: INTERVAL
        ) dd(dat);

    INSERT INTO
        t850 (val, dat)
    SELECT
        array_to_json(
            array(
                SELECT
                    floor(random() * (7000 -2000 + 1) + 2000) :: int
                FROM
                    generate_series(0, 144 * 73 -1)
            )
        ),
        dd.dat
    FROM
        generate_series (
            '2018-01-01' :: timestamp,
            '2020-01-01' :: timestamp,
            '1 day' :: INTERVAL
        ) dd(dat);

    INSERT INTO
        prec (val, dat)
    SELECT
        array_to_json(
            array(
                SELECT
                    floor(random() * (7000 -2000 + 1) + 2000) :: int
                FROM
                    generate_series(0, 144 * 73 -1)
            )
        ),
        dd.dat
    FROM
        generate_series (
            '2018-01-01' :: timestamp,
            '2020-01-01' :: timestamp,
            '1 day' :: INTERVAL
        ) dd(dat);
EOSQL
