from pathlib import Path

from starlette.config import Config
from starlette.datastructures import URL

config = Config(".env")

DEBUG: bool = config("DEBUG", cast=bool, default=False)
POSTGRES_DSN: URL = config("DB_CONNECTION", cast=URL)
VALUES_FILE: Path = config("STORAGE_PATH", cast=Path)

GRASP_COLS = 144
GRASP_ROWS = 74
