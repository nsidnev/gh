from fastapi import FastAPI
from starlette.templating import Jinja2Templates

from app import config, events, routes


def get_application() -> FastAPI:
    app = FastAPI(debug=config.DEBUG)

    app.add_event_handler("startup", events.create_startup_handler(app))
    app.add_event_handler("shutdown", events.create_shutdown_handler(app))

    app.include_router(routes.router)

    app.state.templates = Jinja2Templates(directory="app/templates")

    return app


app = get_application()
