import struct
from datetime import date
from os import PathLike
from typing import List

import aiofiles
import buildpg
from asyncpg.connection import Connection

from app.models import GraspTypes


async def write_positions_in_binary_file(
    filename: PathLike, calculated_positions: List[float]
) -> None:
    output_data = b"".join(
        struct.pack("f", single_position) for single_position in calculated_positions
    )

    async with aiofiles.open(filename, "ab") as out_file:
        await out_file.write(output_data)


async def get_average_for_values(
    connection: Connection, grasp_type: GraspTypes, start_date: date, end_date: date
) -> List[float]:
    query, args = buildpg.render(
        """
    SELECT
        avg(transponed_arrays.element :: numeric)
    FROM
        :table_name,
        LATERAL (
            SELECT
                val ->> length_series.idx element,
                length_series.idx idx
            FROM
                (
                    SELECT
                        generate_series(0, jsonb_array_length(val) - 1)
                ) length_series(idx)
        ) transponed_arrays
    WHERE
        :table_name.dat BETWEEN :start_date
        AND :end_date
    GROUP BY
        transponed_arrays.idx
    ORDER BY
        transponed_arrays.idx;
    """,
        table_name=buildpg.V(grasp_type.name),
        start_date=start_date,
        end_date=end_date,
    )
    calculated_values = await connection.fetch(query, *args)

    return [rec[0] for rec in calculated_values]
