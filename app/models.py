from enum import Enum
from typing import Dict


class GraspTypes(str, Enum):  # noqa: WPS:600
    h500: str = "h500"
    merd: str = "merd"
    t850: str = "t850"
    prec: str = "prec"

    @classmethod
    def to_dict(cls) -> Dict[str, str]:
        return {enum_value.name: enum_value.value for enum_value in cls}
