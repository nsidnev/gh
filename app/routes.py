from datetime import date

from asyncpg.connection import Connection
from fastapi import APIRouter, Depends, FastAPI, Form
from starlette.requests import Request
from starlette.responses import FileResponse, Response

from app import config, dependencies, models, services

router = APIRouter()


@router.get("/", name="index")
async def index(request: Request) -> Response:
    templates = request.app.state.templates
    return templates.TemplateResponse(
        name="index.html",
        context={
            "request": request,
            "types": models.GraspTypes.to_dict(),
            "errors": [],
        },
    )


@router.post("/calculate-position")
async def calculation(  # noqa: WPS:211
    *,
    grasp_type: models.GraspTypes = Form(...),
    start_date: date = Form(...),
    end_date: date = Form(...),
    request: Request,
    app: FastAPI = Depends(dependencies.get_application),
    connection: Connection = Depends(dependencies.get_db_connection),
) -> Response:
    templates = app.state.templates
    if start_date > end_date:
        return templates.TemplateResponse(
            name="index.html",
            context={
                "request": request,
                "types": models.GraspTypes.to_dict(),
                "errors": ["Дата начала должна быть раньше даты завершения"],
            },
        )

    positions = await services.get_average_for_values(
        connection, grasp_type, start_date, end_date
    )

    await services.write_positions_in_binary_file(config.VALUES_FILE, positions)

    # here should be PathLike
    return FileResponse(config.VALUES_FILE, filename=config.VALUES_FILE)  # type: ignore
