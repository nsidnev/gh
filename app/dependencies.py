from asyncpg.connection import Connection
from fastapi import Depends, FastAPI
from starlette.requests import Request


def get_application(request: Request) -> FastAPI:
    return request.app


def get_db_connection(app: FastAPI = Depends(get_application)) -> Connection:
    return app.state.connection
