from typing import Callable

from asyncpg import connect
from fastapi import FastAPI

from app import config


def create_startup_handler(app: FastAPI) -> Callable:
    async def on_app_startup() -> None:
        app.state.connection = await connect(str(config.POSTGRES_DSN))

    return on_app_startup


def create_shutdown_handler(app: FastAPI) -> Callable:
    async def on_app_shutdown() -> None:
        await app.state.connection.close()

    return on_app_shutdown
